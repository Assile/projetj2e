<%-- 
    Document   : Shop
    Created on : Feb 24, 2020, 5:10:46 PM
    Author     : cheri
--%>


<%@page import="ModelEntites.Vente"%>
<%@page import="ModelEntites.produit"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ModelEntites.categorie"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

     
    					<li><a href="#"> <%
                                              // cat1=listeOfCategorie.get(0);
                                               
                                                
                                                
                                                %> </a></li>
                                       
                                        
                                        <!DOCTYPE html>
<html lang="en">
  <head>
    <title>Vegefoods - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body class="goto-here">
		<div class="py-1 bg-primary">
    	<div class="container">
    		<div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
	    		<div class="col-lg-12 d-block">
		    		<div class="row d-flex">
		    			<div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
						    <span class="text">+ 1235 2355 98</span>
					    </div>
					    <div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
						    <span class="text">youremail@email.com</span>
					    </div>
					    <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
						    <span class="text">3-5 Business days delivery &amp; Free Returns</span>
					    </div>
				    </div>
			    </div>
		    </div>
		  </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="/EpicerieFinal/ServIndex">Vegefoods</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>
 <form method="post" action="ServPanier" name="formP">
	      <div class="collapse navbar-collapse" id="ftco-nav">
                  
	        <ul class="navbar-nav ml-auto">
	          <li class="nav-item"><a href="index.html" class="nav-link">Home</a></li>
	          <li class="nav-item active dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shop</a>
              <div class="dropdown-menu" aria-labelledby="dropdown04">
              	<a class="dropdown-item" href="shop.html">Shop</a>
              	<a class="dropdown-item" href="wishlist.html">Wishlist</a>
                <a class="dropdown-item" href="product-single.html">Single Product</a>
                <a class="dropdown-item" href="/EpicerieFinal/ServPanier">Cart</a>
                <a class="dropdown-item" href="checkout.html">Checkout</a>
              </div>
              
            </li>
	          <li class="nav-item"><a href="about.html" class="nav-link">About</a></li>
	          <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li>
	          <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li>
                <%   List<List> listeTotal=new ArrayList();
   
                                           listeTotal=(List)request.getAttribute("categorie");
                                           
                                            


  produit prod1=(produit)request.getAttribute("produit");
 
                                            
 
                                            List<produit> listeOfproduit=listeTotal.get(0);
                                             List<categorie>listeOfCat =listeTotal.get(1);
                                             List<Vente> listOfVente=(List)request.getAttribute("vente");
                                             
                                             int size=listOfVente.size();

                  
                 
                 %>
	          <li class="nav-item cta cta-colored"><a class="nav-link" onClick="document.forms.formP.submit();"><span class="icon-shopping_cart"></span>[<% out.println(size); %>]</a></li>
                   <li><a href="/EpicerieFinal/ServLogout " class="py-2 d-block">Logout</a></li>
                  

	        </ul>
                  
	      </div>
                   </form>
	    </div>
	  </nav>
    <!-- END nav -->

    <div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="/EpicerieFinal/ServIndex">Home</a></span> <span class="mr-2"><a href="/EpicerieFinal/ServIndex">Product</a></span> <span>Product Single</span></p>
            <h1 class="mb-0 bread">Product Single</h1>
          </div>
        </div>
      </div>
    </div>
    

<div class="row justify-content-center">
    			<div class="col-md-10 mb-5 text-center">
    				<ul class="product-category">
                                    
    		<%
    
    
        for(int j=0;j<listeOfCat.size();j++)
            {%>
            <li>  <form method="post" action="ServAct" name="form3<% out.print(j); %>">
                    
                    
                    <a onClick="document.forms.form3<% out.print(j); %>.submit();" > <%    out.println( listeOfCat.get(j).getNomCat());%>
                    <input  type="Hidden"   style="border-radius: 100%;"name="choix" value=<% out.println(listeOfCat.get(j).getNomCat()); %> >
                    </a>
                </form>
            </li>
                                        
                                        
    			<%		} %>
    				</ul>
    			</div>
    		</div>
    		
                     <section class="ftco-section">
                         
                         
                         <div class="container">
    		<div class="row">
    			<div class="col-lg-6 mb-5 ftco-animate">
                            <a href="images/product-1.jpg" class="image-popup"><div id="imageP"><img src="images/<% 
  if(prod1!=null){
      out.println(prod1.getUrlImageProduit()) ; 
  }
  else{
      out.println(listeOfproduit.get(0).getUrlImageProduit());
  }
                            
                                                                                                     %>" class="img-fluid" alt="Colorlib Template"></div></a>
    			</div>
    			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
                            <h3><div id="nomP"><% 
    if(prod1!=null){
      out.println(prod1.getNomProduit()) ; 
  }
  else{
       out.println(listeOfproduit.get(0).getNomProduit());
  }
                                
                               
                                
                            %></div></h3>
    				<div class="rating d-flex">
							<p class="text-left mr-4">
								<a href="#" class="mr-2">5.0</a>
								<a href="#"><span class="ion-ios-star-outline"></span></a>
								<a href="#"><span class="ion-ios-star-outline"></span></a>
								<a href="#"><span class="ion-ios-star-outline"></span></a>
								<a href="#"><span class="ion-ios-star-outline"></span></a>
								<a href="#"><span class="ion-ios-star-outline"></span></a>
							</p>
							<p class="text-left mr-4">
								<a href="#" class="mr-2" style="color: #000;">100 <span style="color: #bbb;">Rating</span></a>
							</p>
							<p class="text-left">
								<a href="#" class="mr-2" style="color: #000;">500 <span style="color: #bbb;">Sold</span></a>
							</p>
						</div>
    				<p class="price"><span>$<% 
                                    
   if(prod1!=null){
      out.println(prod1.getPrix()) ; 
  }
  else{
           out.println(listeOfproduit.get(0).getPrix());
  }
                               
                                    
                                %></span></p>
    				<p><%
                                       if(prod1!=null){
      out.println(prod1.getDescProduit()) ; 
  }
  else{
           out.println(listeOfproduit.get(0).getDescProduit());
  }
                                    
                                    
                                    
                                %>
						</p>
						<div class="row mt-4">
							<div class="col-md-6">
								<div class="form-group d-flex">
		              <div class="select-wrap">
	                  <div class="icon"><span class="ion-ios-arrow-down"></span></div>
	                  <select name="" id="" class="form-control">
	                  	<option value="">Small</option>
	                    <option value="">Medium</option>
	                    <option value="">Large</option>
	                    <option value="">Extra Large</option>
	                  </select>
	                </div>
		            </div>
							</div>
							<div class="w-100"></div>
							<div class="input-group col-md-6 d-flex mb-3">
	             	<span class="input-group-btn mr-2">
	                	<button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
	                   <i class="ion-ios-remove"></i>
	                	</button>
	            		</span>
	             	<input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
	             	<span class="input-group-btn ml-2">
	                	<button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
	                     <i class="ion-ios-add"></i>
	                 </button>
	             	</span>
	          	</div>
	          	<div class="w-100"></div>
	          	<div class="col-md-12">
	          		<p style="color: #000;">600 kg available</p>
	          	</div>
          	</div>
          	<p><a href="/EpicerieFinal/ServPanier" class="btn btn-black py-3 px-5">Add to Cart</a></p>
    			</div>
    		</div>
    	</div>
                    </section>
       
    	
                    <section class="ftco-section">
<div class="container">
    		<div class="row">
    			
<%
    
    
        for(int i=0;i<listeOfproduit.size();i++)
            {%>
    		 
    			
            <div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="product">
    					<a href="#" class="img-prod"  ><img class="img-fluid" src="images/<% out.println(listeOfproduit.get(i).getUrlImageProduit()); %>" alt="images/<% out.println(listeOfproduit.get(i).getNomProduit()); %>">
	    					<div class="overlay"></div>
	    				</a>
    					<div class="text py-3 pb-4 px-3 text-center">
                                            <h3><a href="#"><div id="nomProd"><% out.println(listeOfproduit.get(i).getNomProduit()); %></div></a></h3>
    						<div class="d-flex">
    							<div class="pricing">
		    						<p class="price"><span>$<% out.println(listeOfproduit.get(i).getPrix()); %></span></p>
		    					</div>
	    					</div>
    						<div class="bottom-area d-flex px-3">
	    						<div class="m-auto d-flex">
                                                            <div class="details">
	    							<a   class="add-to-cart d-flex justify-content-center align-items-center text-center" >
                                                                    
                                                                 
                                                                     <form method="post" action="servCat" name="form<% out.print(i); %>">
                                                                    <div  class="add-to-cart d-flex justify-content-center align-items-center text-center" onClick="document.forms.form<% out.print(i); %>.submit();" >
                                                                   <input  type="Hidden"   style="border-radius: 100%;"name="choixProd" value=<% out.println(listeOfproduit.get(i).getIdProduit()); %> >
                                                                   
                                                                   <span><i class="ion-ios-menu"></i></span>
                                                                    </div>
                                                                     </form>
                                                                   
	    								
	    							</a>
                                                                        
                                                            </div>
                                                                                <form method="post" action="ServVente" name="form1<% out.print(i); %>">
	    							<a class="buy-now d-flex justify-content-center align-items-center mx-1" onClick="document.forms.form1<% out.print(i); %>.submit();">
                                                           <input  type="Hidden"   style="border-radius: 100%;"name="choixVente" value=<% out.println(listeOfproduit.get(i).getIdProduit()); %> >
	    								<span><i class="ion-ios-cart"></i></span>
                                                           
	    							</a>
                                                                 </form>
	    							<a href="#" class="heart d-flex justify-content-center align-items-center ">
	    								<span><i class="ion-ios-heart"></i></span>
	    							</a>
    							</div>
    						</div>
    					</div>
    				</div>
    			
    			
    		
    		
    		
            </div>
                                                          
    	
                                                                        
<%}%>

</div>
</div>
 

    </sect
		<section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
      
      <div class="container py-4">
        <div class="row d-flex justify-content-center py-5">
          <div class="col-md-6">
          	<h2 style="font-size: 22px;" class="mb-0">Subcribe to our Newsletter</h2>
          	<span>Get e-mail updates about our latest shops and special offers</span>
          </div>
          <div class="col-md-6 d-flex align-items-center">
            <form action="#" class="subscribe-form">
              <div class="form-group d-flex">
                <input type="text" class="form-control" placeholder="Enter email address">
                <input type="submit" value="Subscribe" class="submit px-3">
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <footer class="ftco-footer ftco-section">
      <div class="container">
      	<div class="row">
      		<div class="mouse">
						<a href="#" class="mouse-icon">
							<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
						</a>
					</div>
      	</div>
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Vegefoods</h2>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Shop</a></li>
                <li><a href="#" class="py-2 d-block">About</a></li>
                <li><a href="#" class="py-2 d-block">Journal</a></li>
                <li><a href="#" class="py-2 d-block">Contact Us</a></li>
                <li><a href="#" class="py-2 d-block">Logout</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-4">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Help</h2>
              <div class="d-flex">
	              <ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
	                <li><a href="#" class="py-2 d-block">Shipping Information</a></li>
	                <li><a href="#" class="py-2 d-block">Returns &amp; Exchange</a></li>
	                <li><a href="#" class="py-2 d-block">Terms &amp; Conditions</a></li>
	                <li><a href="#" class="py-2 d-block">Privacy Policy</a></li>
	              </ul>
	              <ul class="list-unstyled">
	                <li><a href="#" class="py-2 d-block">FAQs</a></li>
	                <li><a href="#" class="py-2 d-block">Contact</a></li>
	              </ul>
	            </div>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Have a Questions?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@yourdomain.com</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
						  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>

  <script>
      
      function AffichageElement(){
          var Nom = document. getElementById('nomProd'). textContent
                alert(Nom);
                   
               }
              
    //document.getElementsByClassName("details")[0].onclick=function(){AffichageElement()};
              
      
     

  var y = document.getElementsByClassName("details");
  var i;
  for (i = 0; i < y.length; i++) {
     //document.getElementsByClassName("details")[i].onclick=function(){AffichageElement()};
  }


		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>
    
  </body>
</html>