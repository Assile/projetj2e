/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ModelDao.CategorieDAO;
import ModelDao.ProduitDAO;
import ModelDao.VenteDAO;
import ModelEntites.Vente;
import ModelEntites.categorie;
import ModelEntites.produit;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author cheri
 */
public class servCat extends HttpServlet {

CategorieDAO daoCat;
 ProduitDAO dao;
  produit prod1;
  VenteDAO daoVente;
 
    public void init(){
        dao=new ProduitDAO();
     daoCat = new CategorieDAO();
   daoVente = new VenteDAO();
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
     
       
       
        
       
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
        
        
        
        
         init();
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out=response.getWriter();
           String choixProd=request.getParameter("choixProd");
             prod1=dao.getProduitsParID(choixProd);
      List<produit> listeOfProduit=dao.getProduitsParCat(prod1.getIdCat());
  
          ArrayList<List> listeTotal=new ArrayList();
          listeTotal.add(listeOfProduit);
           List<categorie>listeOfCategorie=daoCat.getAllCategorie();
            listeTotal.add(listeOfCategorie);
           // String user=(String) session.getAttribute("user");
           HttpSession session=request.getSession(false);
               String idUser=(String) session.getAttribute("idUser");
       
       
           listeTotal.add(listeOfProduit);
           
        request.setAttribute("categorie", listeTotal);
          request.setAttribute("produit", prod1);
       
            List<Vente> listOfVente=daoVente.getALLVente();
        
         request.setAttribute("vente", listOfVente);
          
          
        request.getRequestDispatcher("Shop.jsp").forward(request, response);
       
       
       
       
      
        
         
           
      
        
        
   
       
          
           
        
           
           
        
          
       
      
          
           
      
   
       
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
