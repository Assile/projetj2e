/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ModelDao.UtilisateurDAO;
import ModelDao.VenteDAO;
import ModelEntites.Utilisateur;
import ModelEntites.Vente;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author cheri
 */
public class ServLogin extends HttpServlet {
   UtilisateurDAO dao;
   
       public void init(){
      dao= new UtilisateurDAO();
       String idUser;
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServLogin</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServLogin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        init();
        String reponseLogin;
        String user=request.getParameter("user");
        String pw=request.getParameter("pw");
        int intLogin=dao.LoginUser(user,pw);
          UtilisateurDAO daoUtilisateur; 
         Utilisateur util1=new Utilisateur();
         String idUser;
         
        
       switch (intLogin) {
           case 1:
               reponseLogin="Mot de passe et User faux";
               request.setAttribute("Reponse", reponseLogin);
               request.getRequestDispatcher("Login.jsp").forward(request, response);
               break;
           case 2:
               
               
             
               HttpSession session = request.getSession();
               session.setAttribute("user", user);
               
              
             daoUtilisateur=new UtilisateurDAO();
             util1= daoUtilisateur.RechercheIDparUser(user);
             idUser=util1.getIdUser();
            
            
             session.setAttribute("idUser", idUser);
             
               VenteDAO daoVente;
               daoVente = new VenteDAO();
                 List<Vente> listOfVente=daoVente.getVentesParid(idUser);
                  request.setAttribute("vente", listOfVente);
              
                 request.getRequestDispatcher("IndexMembre.jsp").forward(request, response);
               break;
           case 3:
               reponseLogin="Mot de passe faux";
               request.setAttribute("Reponse", reponseLogin);
               request.getRequestDispatcher("Login.jsp").forward(request, response);
               break;
           case 4:
               reponseLogin="on est dans le catch";
               request.setAttribute("Reponse", reponseLogin);
               request.getRequestDispatcher("Login.jsp").forward(request, response);
           default:
               reponseLogin="Erreur dans le code Login";
              
               break;
       }
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
