/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author cheri
 */
public class SingletonConnexion {
        private static Connection connexion;

    public static Connection getConnexion() {
        if(connexion!=null){
         return connexion;   
        }
        else{
           
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url="jdbc:mysql://localhost:3306/magasin?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            String user="root";
            String pw="";
            connexion=DriverManager.getConnection(url,user,pw);
            
        } catch (ClassNotFoundException ex) {
ex.printStackTrace();        } catch (SQLException ex) {
ex.printStackTrace();        }
       
    }
        return connexion; 
        }
    
}
