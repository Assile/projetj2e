/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelDao;

import ModelEntites.Vente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cheri
 */
public class VenteDAO  implements IVente{

    @Override
    public List<Vente> getALLVente() {
         Connection connexion=SingletonConnexion.getConnexion();
       PreparedStatement ps;
       ArrayList<Vente>listeOfVente=new ArrayList<>();
     try {
         ps=connexion.prepareStatement("SELECT * FROM vente ;");
        ResultSet rs= ps.executeQuery();
           while(rs.next()){
               Vente v=new Vente();
               v.setIdVente(rs.getInt("idVente"));
                v.setCodeProduit(rs.getString("codeProduit"));
                v.setIdUsr(rs.getString("codeUtilisateur"));
               v.setQte(rs.getInt("Qte"));
               v.setPrixVente(rs.getDouble("prixVente"));
               listeOfVente.add(v);
     }
     }catch (SQLException ex) {
         Logger.getLogger(VenteDAO.class.getName()).log(Level.SEVERE, null, ex);
     }
     return listeOfVente;
    }

    @Override
    public List<Vente> getVentesParid(String codeUtilisateur) {
         Connection connexion=SingletonConnexion.getConnexion();
    PreparedStatement ps;
       ArrayList<Vente>listeOfVente=new ArrayList();
     try {
         ps=connexion.prepareStatement("SELECT*from produit ,vente WHERE produit.idProd=vente.codeProduit and vente.codeUtilisateur=?");
          ps.setString(1, codeUtilisateur);
           ResultSet rs= ps.executeQuery();
           while(rs.next()){
               Vente p=new Vente();
               p.setIdVente(rs.getInt("idVente"));
               p.setCodeProduit(rs.getString("codeProduit"));
               p.setIdUsr(rs.getString("codeUtilisateur"));
                 p.setQte(rs.getInt("Qte"));
               p.setPrixVente(rs.getDouble("prixVente"));
               listeOfVente.add(p);
     }
     }catch (SQLException ex) {
         Logger.getLogger(VenteDAO.class.getName()).log(Level.SEVERE, null, ex);
     }
     return listeOfVente;
    }

    @Override
    public Vente getVentesParID(String idVente) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void ajouterVente(Vente p) {
         Connection connexion=SingletonConnexion.getConnexion();
        PreparedStatement ps;
        try {
            ps=connexion.prepareStatement("insert into Vente values(?,?,?,?,?)");
            ps.setInt(1, p.getIdVente());
            ps.setString(2, p.getCodeProduit());
            ps.setString(3, p.getIdUsr());
            ps.setInt(4, p.getQte());
            ps.setDouble(5,p.getprixVente());
            ps.executeUpdate();

        } catch (SQLException ex) {
ex.printStackTrace();            
        }
    }

    @Override
    public void supprimerVente(int idVente) {
         Connection connexion=SingletonConnexion.getConnexion();
        PreparedStatement ps;
        try {
            ps=connexion.prepareStatement("delete from Vente where idVente=?");
            ps.setInt(1, idVente);
            ps.executeUpdate();
        } catch (SQLException ex) {
ex.printStackTrace();      
        }
    }
    }
    
    

