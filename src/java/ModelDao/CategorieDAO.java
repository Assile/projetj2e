/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelDao;

import ModelEntites.categorie;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cheri
 */
public class CategorieDAO implements ICategorieDAO {
    Connection connexion=SingletonConnexion.getConnexion();

    @Override
    public List<categorie> getAllCategorie() {
        ArrayList<categorie> listeOfCategorie=new ArrayList<>();
        try {
            
            PreparedStatement ps = connexion.prepareStatement("select*from categorie");
            ResultSet rs=ps.executeQuery();
            while(rs.next()){
                categorie c=new categorie();
                c.setIdCat(rs.getString("idCat"));
                c.setNomCat(rs.getString("nomCat"));
                c.setDescCat(rs.getString("descCat"));
                c.setUrlImgCat(rs.getString("urlImgCat"));
                listeOfCategorie.add(c);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategorieDAO.class.getName()).log(Level.SEVERE, null, ex);
           
        }
        
         return listeOfCategorie;
        
        
      
    }

    @Override
    public categorie getCategorieparNom(String nomCat) {
        
         categorie cat1=new categorie();
         
        try {
            
            PreparedStatement ps = connexion.prepareStatement("select*from categorie where nomCat=?");
            ps.setString(1, nomCat);
            ResultSet rs=ps.executeQuery();
            
            while(rs.next()){
                
                cat1.setIdCat(rs.getString("idCat"));
                cat1.setNomCat(rs.getString("nomCat"));
                cat1.setDescCat(rs.getString("descCat"));
                cat1.setUrlImgCat(rs.getString("urlImgCat"));
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategorieDAO.class.getName()).log(Level.SEVERE, null, ex);
           
        }
        
         return cat1;
    }

    @Override
    public void addCat() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deleteCat(String idCat) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
