/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelDao;

import ModelEntites.Utilisateur;

/**
 *
 * @author cheri
 */
public interface IUtilisateurDAO {
   
    int LoginUser(String user,String pw);
    Utilisateur RechercheIDparUser(String user);
    
    
}
