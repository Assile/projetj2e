/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelDao;


import ModelEntites.Vente;
import java.util.List;

/**
 *
 * @author cheri
 */
public interface IVente {
    List<Vente>getALLVente();
    List<Vente>getVentesParid(String idUtilisateur);
    Vente getVentesParID(String idVente);
    void ajouterVente(Vente p);
    void supprimerVente(int idVente );
    
}
