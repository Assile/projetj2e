/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelDao;

import ModelEntites.categorie;
import java.util.List;

/**
 *
 * @author cheri
 */
public interface ICategorieDAO {
    List<categorie> getAllCategorie();
    categorie getCategorieparNom(String nomCat);
    void addCat();
    void deleteCat(String idCat);
    
    
    
}
