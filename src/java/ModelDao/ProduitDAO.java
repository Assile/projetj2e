/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelDao;

import ModelEntites.produit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cheri
 */
public class ProduitDAO implements IProduitDAO {
    
      @Override
    public List<produit> getALLProduits() {
Connection connexion=SingletonConnexion.getConnexion();

PreparedStatement ps;
ArrayList<produit> listeOfProduit=new ArrayList<>();
        try {
            ps=connexion.prepareStatement("Select*from produit;");
            
           ResultSet rs= ps.executeQuery();
           while(rs.next()){
               produit p=new produit();
               p.setIdProduit(rs.getString("idProd"));
               p.setNomProduit(rs.getString("nomProd"));
               p.setPrix(rs.getDouble("prixProd"));
               p.setDescProduit(rs.getString("descProd"));
               p.setUrlImageProduit("UrlImageProd");
               p.setIdCat(rs.getString("idCat"));
               listeOfProduit.add(p);
           }
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

return listeOfProduit;
      }

    @Override
    public List<produit> getProduitsParCat(String idCat) {
Connection connexion=SingletonConnexion.getConnexion();
PreparedStatement ps;
ArrayList<produit> listeOfProduit=new ArrayList<>();
        try {
            ps=connexion.prepareStatement("Select*from Produit where IdCat=?");
            ps.setString(1, idCat);
           ResultSet rs= ps.executeQuery();
           while(rs.next()){
               produit p=new produit();
               p.setIdProduit(rs.getString("idProd"));
               p.setNomProduit(rs.getString("nomProd"));
               p.setPrix(rs.getDouble("prixProd"));
               p.setDescProduit(rs.getString("descProd"));
               p.setUrlImageProduit(rs.getString("UrlImageProd"));
               p.setIdCat(rs.getString("idCat"));
               listeOfProduit.add(p);
           }
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

return listeOfProduit;    }
     @Override
    public produit getProduitsParID(String idProd) {
Connection connexion=SingletonConnexion.getConnexion();
PreparedStatement ps;
produit prod1=new produit();
        try {
            ps=connexion.prepareStatement("Select*from Produit where idProd=?");
            ps.setString(1, idProd);
           ResultSet rs= ps.executeQuery();
           while(rs.next()){
               produit p=new produit();
               p.setIdProduit(rs.getString("idProd"));
               p.setNomProduit(rs.getString("nomProd"));
               p.setPrix(rs.getDouble("prixProd"));
               p.setDescProduit(rs.getString("descProd"));
               p.setUrlImageProduit(rs.getString("UrlImageProd"));
               p.setIdCat(rs.getString("idCat"));
               prod1=p;
           }
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

return prod1;    }

    
        @Override
    public List<produit> getProduitsParUtilisateur(String codeUtilisateur) {
Connection connexion=SingletonConnexion.getConnexion();
PreparedStatement ps;
ArrayList<produit> listeOfProduit=new ArrayList<>();
        try {
            ps=connexion.prepareStatement("SELECT*from produit ,vente WHERE produit.idProd=vente.codeProduit and vente.codeUtilisateur=?");
            ps.setString(1, codeUtilisateur);
           ResultSet rs= ps.executeQuery();
           while(rs.next()){
               produit p=new produit();
               p.setIdProduit(rs.getString("idProd"));
               p.setNomProduit(rs.getString("nomProd"));
               p.setPrix(rs.getDouble("prixProd"));
               p.setDescProduit(rs.getString("descProd"));
               p.setUrlImageProduit(rs.getString("UrlImageProd"));
               p.setIdCat(rs.getString("idCat"));
               listeOfProduit.add(p);
           }
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

return listeOfProduit;    }
    @Override
    public void ajouterProduit(produit p) {
        Connection connexion=SingletonConnexion.getConnexion();
PreparedStatement ps;
        try {
            ps=connexion.prepareStatement("insert into produit values(?,?,?,?,?,?)");
            ps.setString(1, p.getIdProduit());
            ps.setString(2, p.getNomProduit());
            ps.setDouble(3, p.getPrix());
            ps.setString(4,p.getUrlImageProduit());
            ps.setString(5, p.getDescProduit());
            ps.executeUpdate();

        } catch (SQLException ex) {
ex.printStackTrace();            
        }

        
    }

    @Override
    public void supprimerProduit(String idProduit) {
 Connection connexion=SingletonConnexion.getConnexion();
PreparedStatement ps;
        try {
            ps=connexion.prepareStatement("delete from produit where idProd=?");
            ps.setString(1, idProduit);
            ps.executeUpdate();
        } catch (SQLException ex) {
ex.printStackTrace();      
        }
    }
}
