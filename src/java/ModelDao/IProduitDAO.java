/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelDao;

import ModelEntites.produit;
import java.util.List;

/**
 *
 * @author cheri
 */
public interface IProduitDAO {
        List<produit>getALLProduits();
    List<produit>getProduitsParCat(String idCat);
    produit getProduitsParID(String idProd);
   List<produit> getProduitsParUtilisateur(String codeUtilisateur);
    void ajouterProduit(produit p);
    void supprimerProduit(String idProduit );
    
}
