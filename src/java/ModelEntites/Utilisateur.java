/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelEntites;

/**
 *
 * @author cheri
 */
public class Utilisateur {

    public Utilisateur(String idUser, String nom, String prenom, String user, String pw, String email) {
        this.idUser = idUser;
        this.nom = nom;
        this.prenom = prenom;
        this.user = user;
        this.pw = pw;
        this.email = email;
    }

    public Utilisateur(String idUser) {
        this.idUser = idUser;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getPw() {
        return pw;
    }

    public String getEmail() {
        return email;
    }

    public String getUser() {
        return user;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Utilisateur() {
    }
    private String idUser;
    private String nom;
    private String prenom;
    private String user;
    private String pw;
    private String email;
    
}
