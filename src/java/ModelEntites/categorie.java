 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelEntites;

/**
 *
 * @author cheri
 */
public class categorie {

    public String getIdCat() {
        return idCat;
    }

    public String getNomCat() {
        return nomCat;
    }

    public String getDescCat() {
        return descCat;
    }

    public String getUrlImgCat() {
        return urlImgCat;
    }

    public void setIdCat(String idCat) {
        this.idCat = idCat;
    }

    public void setNomCat(String nomCat) {
        this.nomCat = nomCat;
    }

    public void setDescCat(String descCat) {
        this.descCat = descCat;
    }

    public void setUrlImgCat(String urlImgCat) {
        this.urlImgCat = urlImgCat;
    }

    public categorie(String idCat, String nomCat, String descCat, String urlImgCat) {
        this.idCat = idCat;
        this.nomCat = nomCat;
        this.descCat = descCat;
        this.urlImgCat = urlImgCat;
    }

    public categorie() {
    }

    public categorie(String idCat) {
        this.idCat = idCat;
    }
    private String idCat;
    private String nomCat;
    private String descCat;
    private  String urlImgCat;
    
}
