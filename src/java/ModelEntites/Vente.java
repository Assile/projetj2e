/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelEntites;

import java.util.Date;

/**
 *
 * @author cheri
 */
public class Vente {



    public Vente(int idVente) {
        this.idVente = idVente;
    }

    public int getIdVente() {
        return idVente;
    }

    public String getCodeProduit() {
        return codeProduit;
    }

  

    public int getQte() {
        return qte;
    }

    public Double getprixVente() {
        return prixVente;
    }

    public void setIdVente(int idVente) {
        this.idVente = idVente;
    }

    public void setCodeProduit(String codeProduit) {
        this.codeProduit = codeProduit;
    }


    public void setQte(int qte) {
        this.qte = qte;
    }

  

    public Vente() {
    }

    public Vente(int idVente, String idUtilisateur, String codeProduit, int qte, Double prixVente) {
        this.idVente = idVente;
        this.idUtilisateur = idUtilisateur;
        this.codeProduit = codeProduit;
        this.qte = qte;
        this.prixVente = prixVente;
    }


    
  
    
    public String getIdUsr() {
        return idUtilisateur;
    }

    public Double getPrixVente() {
        return prixVente;
    }

    public void setIdUsr(String idUsr) {
        this.idUtilisateur = idUsr;
    }

    public void setPrixVente(Double prixVente) {
        this.prixVente = prixVente;
    }
     int idVente;
    String idUtilisateur;
    String codeProduit;
  
    int qte;
    Double prixVente;
     
    
}
