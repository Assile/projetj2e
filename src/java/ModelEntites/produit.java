/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelEntites;

/**
 *
 * @author cheri
 */
public class produit {
        public String getIdProduit() {
        return idProduit;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public double getPrix() {
        return Prix;
    }

    public String getDescProduit() {
        return descProduit;
    }

    public String getIdCat() {
        return idCat;
    }

    public void setIdProduit(String idProduit) {
        this.idProduit = idProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public void setPrix(double Prix) {
        this.Prix = Prix;
    }

    public void setDescProduit(String descProduit) {
        this.descProduit = descProduit;
    }

    public void setIdCat(String idCat) {
        this.idCat = idCat;
    }

    public produit(String idProduit, String nomProduit, double Prix, String descProduit, String idCat) {
        this.idProduit = idProduit;
        this.nomProduit = nomProduit;
        this.Prix = Prix;
        this.descProduit = descProduit;
        this.idCat = idCat;
    }

    public produit(String idProduit) {
        this.idProduit = idProduit;
    }

    public produit(String idProduit, String idCat) {
        this.idProduit = idProduit;
        this.idCat = idCat;
    }

    public produit() {
    }
      public void setUrlImageProduit(String urlImageProduit) {
        this.urlImageProduit = urlImageProduit;
    }

    public String getUrlImageProduit() {
        return urlImageProduit;
    }

    public produit(String idProduit, String nomProduit, double Prix, String descProduit, String urlImageProduit, String idCat) {
        this.idProduit = idProduit;
        this.nomProduit = nomProduit;
        this.Prix = Prix;
        this.descProduit = descProduit;
        this.urlImageProduit = urlImageProduit;
        this.idCat = idCat;
    }

    
    private String idProduit;
    private String nomProduit;
    private double Prix;
    private String descProduit;
     private String urlImageProduit;
    private String idCat;
   
    
}
